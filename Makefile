all:
	
	gcc -g -c -Wall socketserv.c -o socketserv.o
	
	gcc -g -c -Wall socketclien.c -o socketclien.o
	
	gcc -g -Wall socketserv.o  -o servidor
	
	gcc -g -Wall socketclien.o  -o cliente	

clean:

	rm *.o
	
	rm servidor
	
	rm cliente
